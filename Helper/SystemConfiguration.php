<?php

namespace Brukeo\Core\Helper;

class SystemConfiguration
{

    protected \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;
    protected \Magento\Framework\Encryption\EncryptorInterface $encryptor;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
    }

    public function getGoogleApiKey(): string
    {
        $value = (string) $this->scopeConfig->getValue(
            \Brukeo\Core\Helper\Constants::BRUKEO_GOOGLE_CONFIGURATION_API_KEY_SYS_PATH,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT
        );

        return $this->encryptor->decrypt($value);
    }

}
